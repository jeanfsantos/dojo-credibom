import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

export type TVehicle = {
  id: string;
};

@Injectable()
export class VehicleService {
  getPrivateVehicles() {
    return of([]).pipe(delay(500));
  }

  getStandVehicles() {
    return of([]).pipe(delay(500));
  }

  getStandVehicle(id: string) {
    return of({}).pipe(delay(500));
  }

  getPrivateVehicle(id: string) {
    return of({}).pipe(delay(500));
  }

  createPrivateVehicle(vehicle: TVehicle) {
    return of({}).pipe(delay(500));
  }

  createStandVehicle(vehicle: TVehicle) {
    return of({}).pipe(delay(500));
  }

  updatePrivateVehicle(vehicle: TVehicle) {
    return of({}).pipe(delay(500));
  }

  updateStandVehicle(vehicle: TVehicle) {
    return of({}).pipe(delay(500));
  }

  deletePrivateVehicle(id: string) {
    return of(null).pipe(delay(500));
  }

  deleteStandVehicle(id: string) {
    return of(null).pipe(delay(500));
  }
}
