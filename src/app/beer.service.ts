import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

type Volume = {
  value: number;
  unit: string;
};

type BoilVolume = {
  value: number;
  unit: string;
};

type Temp = {
  value: number;
  unit: string;
};

type MashTemp = {
  temp: Temp;
  duration: number;
};

type Temp2 = {
  value: number;
  unit: string;
};

type Fermentation = {
  temp: Temp2;
};

type Method = {
  mash_temp: MashTemp[];
  fermentation: Fermentation;
  twist?: any;
};

type Amount = {
  value: number;
  unit: string;
};

type Malt = {
  name: string;
  amount: Amount;
};

type Amount2 = {
  value: number;
  unit: string;
};

type Hop = {
  name: string;
  amount: Amount2;
  add: string;
  attribute: string;
};

type Ingredients = {
  malt: Malt[];
  hops: Hop[];
  yeast: string;
};

export type TBeer = {
  id: number;
  name: string;
  tagline: string;
  first_brewed: string;
  description: string;
  image_url: string;
  abv: number;
  ibu: number;
  target_fg: number;
  target_og: number;
  ebc: number;
  srm: number;
  ph: number;
  attenuation_level: number;
  volume: Volume;
  boil_volume: BoilVolume;
  method: Method;
  ingredients: Ingredients;
  food_pairing: string[];
  brewers_tips: string;
  contributed_by: string;
};

@Injectable({
  providedIn: 'root'
})
export class BeerService {
  constructor(private http: HttpClient) {}

  beers(): Observable<TBeer[]> {
    return this.http
      .get<TBeer[]>('https://api.punkapi.com/v2/beers')
      .pipe(map((beers) => beers.slice(0, 7)));
  }
}
