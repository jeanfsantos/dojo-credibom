import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

type TPost = {
  userId: number;
  id: number;
  title: string;
  body: string;
};

type TTodo = {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
};

@Injectable()
export class PlaceholderService {
  posts$: Observable<TPost[]>;

  constructor(private http: HttpClient) {}

  getPosts(): Observable<TPost[]> {
    return this.http.get<TPost[]>('https://jsonplaceholder.typicode.com/posts');
  }

  getTodos(value: string): Observable<TTodo[]> {
    return this.http.get<TTodo[]>('https://jsonplaceholder.typicode.com/todos');
  }
}
