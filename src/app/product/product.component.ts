import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TBeer } from '../beer.service';
import { Store } from '@ngrx/store';
import { addBeer } from '../product.actions';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {
  @Input() beer: TBeer;

  constructor(private store: Store<{ beers: TBeer[] }>) {}

  onClick() {
    this.store.dispatch(addBeer({ beer: this.beer }));
  }
}
