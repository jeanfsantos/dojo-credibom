import { createAction, props } from '@ngrx/store';
import { TBeer } from './beer.service';

export const addBeer = createAction('[Beer] Add', props<{ beer: TBeer }>());
export const deleteBeer = createAction('[Beer] Delete', props<{ beer: TBeer }>());
