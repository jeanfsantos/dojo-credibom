import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TBeer } from '../beer.service';
import { deleteBeer } from '../product.actions';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {
  beers$: Observable<TBeer[]>;

  constructor(private store: Store<{ beers: TBeer[] }>) {
    this.beers$ = this.store.select('beers');
  }

  trackByFn(index, beer: TBeer) {
    return beer.id;
  }

  onClick(beer: TBeer) {
    this.store.dispatch(deleteBeer({ beer }));
  }
}
