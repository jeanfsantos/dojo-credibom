import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BeerService, TBeer } from './beer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  beers$: Observable<TBeer[]>;

  constructor(private beerService: BeerService) {}

  ngOnInit() {
    this.getBeers();
  }

  getBeers() {
    this.beers$ = this.beerService.beers();
  }

  trackByFn(index, beer: TBeer) {
    return beer.id;
  }
}
