import { createReducer, on } from '@ngrx/store';
import { TBeer } from './beer.service';
import { addBeer, deleteBeer } from './product.actions';

export const initialState: TBeer[] = [];

const _addBeer = (state: TBeer[], action: { type: string; beer: TBeer }) => {
  const hasBeer = state.some((beer) => beer.id === action.beer.id);
  if (hasBeer) {
    return state;
  }
  return [...state, action.beer];
};

const _deleteBeer = (state: TBeer[], action: { type: string; beer: TBeer }) => {
  const beers = state.filter((beer) => beer.id !== action.beer.id);
  return beers;
};

const _beersReducer = createReducer(
  initialState,
  on(addBeer, _addBeer),
  on(deleteBeer, _deleteBeer)
);

export function beersReducer(state: TBeer[], action: { type: string; beer: TBeer }) {
  return _beersReducer(state, action);
}
